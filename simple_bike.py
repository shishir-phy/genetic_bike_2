import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import distance
import matplotlib.animation as animation

"""initializing the needed arrays"""
# bike_state=np.array([[1,1,0,0],[-1,1,0,0], [-2,-3,0,0],[3,-1,0,0]], float)                         #this should be removed and get it as input
bike_state=np.array([[1,4,0,0],[-1,4,0,0], [-2,0,0,0],[3,2,0,0]], float)
bike_state_pos=np.array([[1,1],[-1,1], [-2,-3],[3,-1]], float)
print("bike state is {}".format(bike_state))
bike_x_components=np.zeros((4,1))
bike_x_components_array=[0,0,0,0]
bike_y_components_array=[0,0,0,0]
bike_y_components=np.zeros((4,1))
forces=np.zeros((4,4))
distance_diagonal_ones=np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]])

"""spring properties"""
spring_constant=5
spring_damping=0.8
rest_lenghts=np.array([[0,2,3,2],[2,0,2,2.5],[3,2,0,1.5],[2,2.5,1.5,0]])    #this must come from the initialization phase
# print(rest_lenghts)

fig=plt.plot()
plt.ylim((-1,10))
plt.xlim((-3,20))

"""we can start the iteration from here"""
dt=0.2
for t in np.arange(1,100,dt):
    """introducing the x and y states"""
    touching_surface=bike_state[:,1]<0
    bike_state[touching_surface,1]=0
    if bike_state[1,1]<0.1:
        bike_state[1,2]=2                               #the moving wheel has a constant speed when touching ground
    bike_state_pos[:,:]=bike_state[:,:2]
    bike_x_components[:,0]=bike_state[:,0]
    bike_x_components_array[:]=bike_state[:,0]
    bike_y_components_array[:]=bike_state[:,1]
    print("bike_x component is: \n{}".format(bike_x_components))
    bike_y_components[:,0]=bike_state[:,1]

    """introducing the square array of distances between each two point, with ones in the main diagonal"""
    distances=distance.squareform(distance.pdist(bike_state_pos)) + distance_diagonal_ones
    print("distances is:\n{}".format(distances))



    """the distances components, needed for calculating the forces components"""
    distances_x=distance.squareform(distance.pdist(bike_x_components))
    # print("distances_x is:\n{}".format(distances_x))
    distances_y=distance.squareform(distance.pdist(bike_y_components))
    # print("distances_y is:\n{}".format(distances_y))


    """calculating the forces on the points in x and y direction"""
    # print((((distances-rest_lenghts)*spring_constant)*(distances_x/distances)))
    # forces_x_total=(np.sum((((distances-rest_lenghts)*spring_constant)*(distances_x/distances)),axis=0))
    forces_x=((distances-rest_lenghts)*spring_constant)*(distances_x/distances)
    forces_y=((distances-rest_lenghts)*spring_constant)*(distances_y/distances)
    print("forces x and y \n{}\n{}".format(forces_x, forces_y))
    signs_x=np.sign(np.subtract.outer(bike_x_components_array, bike_x_components_array))
    signs_y=np.sign(np.subtract.outer(bike_y_components_array, bike_y_components_array))
    forces_x_total=np.sum(forces_x*signs_x, axis=0)
    forces_y_total=np.sum(forces_y*signs_y, axis=0)
    print("forces total \n{}\n{}".format(forces_x_total, forces_y_total))
    print("signs_x is:\n{} , {}".format(signs_x, signs_y))
    # print(forces_x)
    # print(np.transpose(forces_x))
    # forces_y_total=np.sum((((distances-rest_lenghts)*spring_constant)*(distances_y/distances)),axis=0)
    # print(np.transpose(forces_y))

    forces[:,2]=np.transpose(forces_x_total)
    forces[:,3]=np.transpose(forces_y_total)
    # forces=np.c_[np.array([[0,0],[0,0],[0,0],[0,0]]), forces]
    print("this is the forces")
    print(forces)

    """adding the damping and gravitation effect"""
    forces[:,2:]+=-spring_damping*bike_state[:,2:]
    forces[:,3]+= -1

    """updating the velocitis"""
    bike_state=bike_state+forces*dt
    print("bike state=\n{}\n\n".format(bike_state))

    """updating the state"""
    bike_state[:,:2]+=bike_state[:,2:]*dt
    plt.ylim((-1, 10))
    plt.xlim((-3, 20))
    plt.scatter(bike_state_pos[:2,0],bike_state_pos[:2,1], c="r")
    plt.scatter(bike_state_pos[2:,0],bike_state_pos[2:,1], c="b")
    print("bike state is:.....\n{}".format(bike_state))
    plt.show()