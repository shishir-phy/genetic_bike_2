import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

# 100 particles, with x,y,vx,vy
dots = np.random.rand(2,4)

## road_array ##
road_x = np.arange(0, 1, 0.001)
road_y = np.zeros((1000, 1))


# centre on 0.5,0.5
dots[:,2:] -= 0.5
# scale velocities
dots[:,2:] *= 0.01

#making the velocity on x direction 0
dots[:,0] = 0.0
dots[:,1] = 0.25
dots[:,2] *=0.0
dots[:,3] =-0.005

def main():
    fig, ax = plt.subplots()

    ax.set_xlim(-0.01,1)
    ax.set_ylim(-0.01,1)

    xs = dots[:,0]
    ys = dots[:,1]

    line, = ax.plot([], [], 'b.', ms=16)

    def animate(i):
        timestep()
        line.set_data(xs,ys)  # update the data
        return line,

    ani = animation.FuncAnimation(fig, animate,
                                  interval=25,
                                  blit=True)

    # ani.save('basic_animation.mp4', fps=30,
    #          extra_args=['-vcodec', 'libx264'])
    # or
    plt.show()
    print("DOONE!!!")


def timestep():
    dots[:,:2] += dots[:,2:]
    tf_dy=dots[:,1] < 0.0
    dots[tf_dy,3]*=0.0
    dots[tf_dy,2] =0.005
    tf_lx=dots[:,0] < 0.0
    dots[tf_lx,2]*=-1
    tf_uy=dots[:,1] > 1.0
    dots[tf_uy,3]*=-1
    tf_rx=dots[:,0] > 1.0
    #print(dots[:,0])
    print(dots[tf_rx,0])
    dots[tf_rx,2]*=-1
    if(dots[tf_rx,0] > 1.0):
        print(dots[tf_rx,0])
        exit()
    #print(tf)


main()
