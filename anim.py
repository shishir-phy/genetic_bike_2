from matplotlib import pyplot as plt
import numpy as np
from matplotlib import animation
import physics_module as phm

bike_state = np.array([[1,8,0,0],[-1,8,0,0], [-2,4,0,0],[3,6,0,0]], float)
distance=[]

### For Bumpy road ####
road_x = np.arange(-0,50,0.01)

def connectpoints(x1,y1,x2,y2):
    #x1, x2 = x[p1], x[p2]
    #y1, y2 = y[p1], y[p2]
    plt.plot([x1,y1],[x2,y2],'k-')

# Animation function
def animate(i):
    #send bike configuration to Physics module
    phm.update_state(bike_state, 0.01)

    point_1_x = bike_state[0,0]
    point_1_y = bike_state[0,1]
    point_2_x = bike_state[1,0]
    point_2_y = bike_state[1,1]
    point_3_x = bike_state[2,0]
    point_3_y = bike_state[2,1]
    point_4_x = bike_state[3,0]
    point_4_y = bike_state[3,1]

    if(point_1_y <= 0):
        distance.append(point_1_x)
        plt.close()
    if(point_2_y <= 0):
        plt.close()

    if(point_1_x >= 50):
        plt.close()

    point_1.set_xdata(point_1_x)
    point_1.set_ydata(point_1_y)
    point_2.set_xdata(point_2_x)
    point_2.set_ydata(point_2_y)
    point_3.set_xdata(point_3_x)
    point_3.set_ydata(point_3_y)
    point_4.set_xdata(point_4_x)
    point_4.set_ydata(point_4_y)

    line_1_2.set_data([point_1_x,point_2_x],[point_1_y,point_2_y])
    line_2_3.set_data([point_2_x,point_3_x],[point_2_y,point_3_y])
    line_3_4.set_data([point_3_x,point_4_x],[point_3_y,point_4_y])
    line_4_1.set_data([point_4_x,point_1_x],[point_4_y,point_1_y])
    line_1_3.set_data([point_1_x,point_3_x],[point_1_y,point_3_y])
    line_2_4.set_data([point_2_x,point_4_x],[point_2_y,point_4_y])

    return point_1, point_2, point_3, point_4, line_1_2, line_2_3, line_3_4, line_4_1, line_1_3, line_2_4



fig, ax = plt.subplots()

## Loop for simulate 20 bikes for each generation ##
for i in range(20):
    #call GA
    #Get bike configuration
    ax.set_xlim(-2,50)
    ax.set_ylim(-3,50)

    point_1_x = bike_state[0,0]
    point_1_y = bike_state[0,1]
    point_2_x = bike_state[1,0]
    point_2_y = bike_state[1,1]
    point_3_x = bike_state[2,0]
    point_3_y = bike_state[2,1]
    point_4_x = bike_state[3,0]
    point_4_y = bike_state[3,1]


    ## Ploting Inital 4 points of bike ##
    point_1, = plt.plot(point_1_x, point_1_y, 'ro-')
    point_2, = plt.plot(point_2_x, point_2_y, 'ro-')
    point_3, = plt.plot(point_3_x, point_3_y, 'bo-')
    point_4, = plt.plot(point_4_x, point_4_y, 'bo-')

    road, = plt.plot(road_x,np.sin(0.3*road_x))

    line_1_2, = plt.plot([point_1_x,point_1_y],[point_2_x,point_2_y],'k:')
    line_2_3, = plt.plot([point_2_x,point_2_y],[point_3_x,point_3_y],'k:')
    line_3_4, = plt.plot([point_3_x,point_3_y],[point_4_x,point_4_y],'k:')
    line_4_1, = plt.plot([point_4_x,point_4_y],[point_1_x,point_1_y],'k:')
    line_1_3, = plt.plot([point_1_x,point_1_y],[point_3_x,point_3_y],'k:')
    line_2_4, = plt.plot([point_2_x,point_2_y],[point_4_x,point_4_y],'k:')

    ani = animation.FuncAnimation(fig, animate, interval=0.25,blit=False)
    print(distance)
    plt.show()
print("DONE!!!!")
print(distance)

#main()
