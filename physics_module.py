import numpy as np
from scipy.spatial import distance
def calculate_forces(bike_state, rest_lengths=np.array([[0,2,5,2.83],[2,0,4.1,5.3],[5,4.1,0,5.3],[2.83,5.3,5.3,0]]),
                 spring_constant=500, spring_damping=0.8):

    """introducing the x and y states"""
    #touching_surface=bike_state[:,1]<0
    #bike_state[touching_surface,1]=0

    """the moving wheels velocity"""

    h_3=bike_state[3,1]
    h_2=bike_state[2,1]
    for i in range(2,4):
        x=bike_state[i,0]
        if bike_state[i,1]<np.math.sin(0.3*x):
            bike_state[i,3]=22*np.abs(np.math.sin(0.3*np.math.atan(np.math.cos(x))))
            bike_state[i,2]=22*np.abs(np.math.cos(0.3*np.math.atan(np.math.cos(x))))                               #the moving wheel has a constant speed when touching ground
            bike_state[i,1]=np.math.sin(0.3*x)

    bike_state_pos = bike_state[:, :2]

    forces = np.zeros((4, 4))                           #initiating the forces array

    distance_diagonal_ones = np.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])     #to make the 0/0 devision into 0/1 devision

    """the x and y components to calculate the x and distances"""
    bike_x_components = np.zeros((4, 1))
    bike_y_components = np.zeros((4, 1))
    bike_x_components[:, 0] = bike_state[:, 0]
    bike_y_components[:, 0] = bike_state[:, 1]

    """these component arrays are just needed to calculate the sign arrays easily (it must be 1D array)"""
    bike_x_components_array = [0, 0, 0, 0]
    bike_y_components_array = [0, 0, 0, 0]
    bike_x_components_array[:] = bike_state[:, 0]
    bike_y_components_array[:] = bike_state[:, 1]


    """introducing the square array of distances between each two point, with ones in the main diagonal"""
    distances = distance.squareform(distance.pdist(bike_state_pos)) + distance_diagonal_ones
    print("distances is:\n{}".format(distances))

    """the distances components, needed for calculating the forces components"""
    distances_x = distance.squareform(distance.pdist(bike_x_components))
    distances_y = distance.squareform(distance.pdist(bike_y_components))



    """calculating the forces on the points in x and y direction"""
    forces_x = ((distances - rest_lengths) * spring_constant) * (distances_x / distances)   #f(x)=(delta_l)*k*cos(theta)
    forces_y = ((distances - rest_lengths) * spring_constant) * (distances_y / distances)   #f(y)=(delta_l)*k*sin(theta)
    # print("forces x and y \n{}\n{}".format(forces_x, forces_y))

    """this is the signs for relative displacements of each two particles"""
    signs_x = np.sign(np.subtract.outer(bike_x_components_array, bike_x_components_array))
    signs_y = np.sign(np.subtract.outer(bike_y_components_array, bike_y_components_array))

    """total forces on each particle"""
    forces_x_total = np.sum(forces_x * signs_x, axis=0)
    forces_y_total = np.sum(forces_y * signs_y, axis=0)

    """just make the force array the size of the state array"""
    forces[:, 2] = np.transpose(forces_x_total)
    forces[:, 3] = np.transpose(forces_y_total)
    # print("this is the forces")
    # print(forces)

    """adding the damping and gravitation effect"""
    forces[:, 2:] += -spring_damping * bike_state[:, 2:]
    forces[:, 3] += -19.8
    return (forces)


"""updating the state works as a function to change the positions and velocites"""
def update_state(bike_state, timestep):
    forces=calculate_forces(bike_state)
    """updating the velocitis"""
    bike_state += forces * timestep
    print("bike state=\n{}\n\n".format(bike_state))

    """updating the state"""
    bike_state[:, :2] += bike_state[:, 2:] * timestep

    """bouncing condition and implementation"""
   # for i in range(2,4):
   #     path_x_1=bike_state[i,0]
   #     path_x_2=bike_state[i,0]-0.02
   #     path_y_1=np.math.sin(path_x_1)
   #     path_y_2=np.math.sin(path_x_2)
   #     Points= [[path_x_1, path_y_1],[path_x_2, path_y_2]]
   #     if bike_state[i,1]<path_y_1:
   #         bounce(bike_state,Points,part_index=i)
    return(bike_state)


def bounce(bike_state,Points, part_index):
    """this function first makes a velocity vector from the bike_state,
    then it calculates the slope_vector from Points,
    then it calculates the angle between velocity and slope
    it can then rotate the velocity by twice the angle and decrease the normal velocity"""

    velocity_before=bike_state[part_index,2:]
    slope_vector=np.zeros((1,2))
    slope_vector[0]=Points(1,0)-Points(0,0)
    slope_vector[1]=Points(1,1)-Points(0,1)
    slope_unit_vector=slope_vector / np.linalg.norm(slope_vector)
    velocity_unit_vector= velocity_before / np.linalg.norm(velocity_before)
    angle=2*np.arccos(np.clip(np.dot(slope_unit_vector, velocity_unit_vector), -1.0, 1.0))

    """now we rotate the velocity vector"""
    rotation_matrix=[[np.math.cos(angle), -np.math.sin(angle)],[np.math.sin(angle), np.math.cos(angle)]]
    velocity_after=np.matmul(velocity_before, rotation_matrix)
    bike_state[part_index, 2:]=velocity_after
