from scipy.spatial import distance
def rest_length_array(bike_state):
    bike_state_position=bike_state[:, :2]
    rest_length = distance.squareform(distance.pdist(bike_state_position))
    return(rest_length)