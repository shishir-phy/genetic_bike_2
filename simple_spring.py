import numpy as np
import matplotlib.pyplot as plt
position=np.zeros(4)
print("position is {}".format(position))
k=0.5
b=0.1
timestep=0.1
forces=np.zeros(2)
print("forces is {}".format(forces))
fig=plt.plot()
for t in range(1000):
    forces[1] = (-9.8-k*position[1]-b*position[3])
    position[3]+=forces[1]*timestep               # change the velocities
    position[:2]+=position[2:]*timestep           # change the position by velocities
    print("position is now {} and the force is {}".format(position, forces[1]))
    plt.scatter(t, position[3], c="b", marker="*")
plt.show()
