import numpy as np
from scipy.spatial import distance
def calculate_forces(bike_state, timestep, rest_lengths=np.array([[0,2,3,2],[2,0,2,2.5],[3,2,0,1.5],[2,2.5,1.5,0]]),
                 spring_constant=5, spring_damping=0.8):
    """introducing the x and y states"""
    touching_surface = bike_state[:, 1] < 0
    bike_state[touching_surface, 1] = 0

    """the moving wheel velocity"""
    if bike_state[1, 1] < 0.1:
        bike_state[1, 2] = 2  # the moving wheel has a constant speed when touching ground
    bike_state_pos = bike_state[:, :2]

    forces = np.zeros((4, 4))  # initiating the forces array

    distance_diagonal_ones = np.array(
        [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])  # to make the 0/0 devision into 0/1 devision

    """the x and y components to calculate the x and distances"""
    bike_x_components = np.zeros((4, 1))
    bike_y_components = np.zeros((4, 1))
    bike_x_components[:, 0] = bike_state[:, 0]
    bike_y_components[:, 0] = bike_state[:, 1]

    """these component arrays are just needed to calculate the sign arrays easily (it must be 1D array)"""
    bike_x_components_array = [0, 0, 0, 0]
    bike_y_components_array = [0, 0, 0, 0]
    bike_x_components_array[:] = bike_state[:, 0]
    bike_y_components_array[:] = bike_state[:, 1]

    """introducing the square array of distances between each two point, with ones in the main diagonal"""
    distances = distance.squareform(distance.pdist(bike_state_pos)) + distance_diagonal_ones
    print("distances is:\n{}".format(distances))

    """the distances components, needed for calculating the forces components"""
    distances_x = distance.squareform(distance.pdist(bike_x_components))
    distances_y = distance.squareform(distance.pdist(bike_y_components))

    """calculating the forces on the points in x and y direction"""
    forces_x = ((distances - rest_lengths) * spring_constant) * (distances_x / distances)  # f(x)=(delta_l)*k*cos(theta)
    forces_y = ((distances - rest_lengths) * spring_constant) * (distances_y / distances)  # f(y)=(delta_l)*k*sin(theta)
    # print("forces x and y \n{}\n{}".format(forces_x, forces_y))

    """this is the signs for relative displacements of each two particles"""
    signs_x = np.sign(np.subtract.outer(bike_x_components_array, bike_x_components_array))
    signs_y = np.sign(np.subtract.outer(bike_y_components_array, bike_y_components_array))

    """total forces on each particle"""
    forces_x_total = np.sum(forces_x * signs_x, axis=0)
    forces_y_total = np.sum(forces_y * signs_y, axis=0)

    """just make the force array the size of the state array"""
    forces[:, 2] = np.transpose(forces_x_total)
    forces[:, 3] = np.transpose(forces_y_total)
    # print("this is the forces")
    # print(forces)

    """adding the damping and gravitation effect"""
    forces[:, 2:] += -spring_damping * bike_state[:, 2:]
    forces[:, 3] += -9.8

    return(forces)