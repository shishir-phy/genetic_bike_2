def update_state(forces, bike_state, timestep):
    """updating the velocitis"""
    bike_state = bike_state + forces * timestep
    print("bike state=\n{}\n\n".format(bike_state))

    """updating the state"""
    bike_state[:, :2] += bike_state[:, 2:] * timestep

    return(bike_state)